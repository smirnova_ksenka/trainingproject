﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols;
using TrainingProject.Models;
using TrainingProject.Services;
using TrainingProject.Tasks;
using TrainingProject.Tasks.Task1;
using TrainingProject.Tasks.Task2;
using TrainingProject.Tasks.Task5;
using TrainingProject.Tasks.Task7;

namespace TrainingProject.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext db;
        private IConfiguration configuration;
        private MySqlDbService dbService;
        private IOperation operation;
        public HomeController(ApplicationContext context, IConfiguration configuration, IOperation operation)
        {
            this.configuration = configuration;
            this.db = context;
            this.dbService = new MySqlDbService(db);
            this.operation = operation;
        }

        public async Task<string> Task1()
        {
            FileReadingService service = new FileReadingService();
            return await service.ProcessReadAsync();
        }

        public string Task2()
        {
            FileReadingService service = new FileReadingService();
            var a = service.ProcessReadAsync().GetAwaiter().GetResult();
            return a;
        }

        public DateTime GetCurrentDate()
        {
            DateTime dateTime = DateTime.Now;
            return dateTime;
        }

        public string[] GetCurrentDates()
        {
            var uri = new Uri("http://localhost:5001/Home/GetCurrentDate");

            HttpClient httpClient = new HttpClient();

            var task1 = httpClient.GetAsync(uri);
            var task2 = httpClient.GetAsync(uri);
            var task3 = httpClient.GetAsync(uri);
            Task.WaitAll(task1, task2, task3);

            var task4 = task1.Result.Content.ReadAsStringAsync();
            var task5 = task2.Result.Content.ReadAsStringAsync();
            var task6 = task3.Result.Content.ReadAsStringAsync();
            Task.WaitAll(task4, task5, task6);

            string[] s = new string[] { task4.Result, task5.Result, task6.Result };
            return s;
        }

        public string Index()
        {
            return String.Format("Operation was called {0} and My Key is {1}", operation.OperationTime.ToString(), operation.MyKey);
            //Regex reg = new Regex(".*hn.*");
            //var allPersons = dbService.GetAllPersons();

            //// All and Any
            //var areAllJonesFired = allPersons.Where(p => p.Name == "Jones").All(p => p.IsFired == true);
            //var isAnyoneHavePhoneNumberWhichStartsWith123 = allPersons.Where(p => !String.IsNullOrEmpty(p.Phone)).Any(p => p.Phone.StartsWith("123"));

            //// Others
            //var transformListOfPersonsIntoListOfNames = allPersons.Select(p => p.Name);
            //var returnNumberOfPersonsWhoHaveNullForPhoneNumber = allPersons.Count(p => p.Phone == null);
            //var returnFirstPersonWhoHaveNameJordan = allPersons.FirstOrDefault(p => p.Name == "Jordan");
            //var returnFirstPersonWhoseNameMatchesWithRegex = allPersons.FirstOrDefault(p => reg.IsMatch(p.Name));
            //var returnListOfPersonsGroupedByBatchesOf10PersonsInEachBatch = allPersons.Select((x, i) => new { Index = i, Value = x })
            //    .GroupBy(x => x.Index / 5)
            //    .Select(x => x.Select(v => v.Value).ToList())
            //    .ToList();

            //return Json(new
            //{
            //    AreAllJonesFired = areAllJonesFired,
            //    IsAnyoneHavePhoneNumberWhichStartsWith123 = isAnyoneHavePhoneNumberWhichStartsWith123,
            //    TransformListOfPersonsIntoListOfNames = transformListOfPersonsIntoListOfNames,
            //    ReturnNumberOfPersonsWhoHaveNullForPhoneNumber = returnNumberOfPersonsWhoHaveNullForPhoneNumber,
            //    ReturnFirstPersonWhoHaveNameJordan = returnFirstPersonWhoHaveNameJordan,
            //    ReturnFirstPersonWhoseNameMatchesWithRegex = returnFirstPersonWhoseNameMatchesWithRegex,
            //    ReturnListOfPersonsGroupedByBatchesOf10PersonsInEachBatch = returnListOfPersonsGroupedByBatchesOf10PersonsInEachBatch
            //});
        }

        public JsonResult GetAllPersons(int pageSize, int pageNumber)
        {
            var sortedPersonsbyName = dbService.GetAllPersons().OrderBy(p => p.Name);
            IEnumerable<Person> personsPerPages = sortedPersonsbyName.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return Json(personsPerPages);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}