﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingProject.Models
{
    public class Person
    {
        public Person()
        {
        }

        public Person(int id, string name, string phone, bool isFired)
        {
            Id = id;
            Name = name;
            Phone = phone;
            IsFired = isFired;
        }
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool IsFired { get; set; }
    }
}
