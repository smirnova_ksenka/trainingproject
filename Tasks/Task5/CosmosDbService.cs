﻿using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingProject.Models;
using System.Net;

namespace TrainingProject.Tasks.Task5
{
    public class CosmosDbService
    {
        private static string endpointUri;
        private static string primaryKey;
        private static CosmosClient cosmosClient;
        private static Container container;
        public CosmosDbService(IConfiguration configuration)
        {
            endpointUri = configuration["CosmosDbSettings:EndPointUri"];
            primaryKey = configuration["CosmosDbSettings:PrimaryKey"];
            cosmosClient = new CosmosClient(endpointUri, primaryKey, new CosmosClientOptions() { ApplicationName = "CosmosDBDotnetQuickstart" });
            container = cosmosClient.GetContainer(configuration["CosmosDbSettings:DataBaseName"], "Persons");
        }

        public async Task AddPersonToContainerAsync(Person person)
        {
            await container.CreateItemAsync<Person>(person, new PartitionKey(person.Id));
        }

        public async Task<List<Person>> GetAllPersonsAsync()
        {
            var sqlQueryText = "SELECT * FROM c";

            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Person> queryResultSetIterator = container.GetItemQueryIterator<Person>(queryDefinition);

            List<Person> persons = new List<Person>();

            while (queryResultSetIterator.HasMoreResults)
            {
                FeedResponse<Person> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                foreach (Person person in currentResultSet)
                {
                    persons.Add(person);
                }
            }
            return persons;
        }

        public async Task<List<Person>> GetPersonByNameAsync(string name)
        {
            var sqlQueryText = String.Format("SELECT * FROM c WHERE c.Name = {0}", name);

            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Person> queryResultSetIterator = container.GetItemQueryIterator<Person>(queryDefinition);

            List<Person> persons = new List<Person>();

            while (queryResultSetIterator.HasMoreResults)
            {
                FeedResponse<Person> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                foreach (Person person in currentResultSet)
                {
                    persons.Add(person);
                }
            }
            return persons;
        }
    }
}
