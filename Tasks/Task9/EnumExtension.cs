﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingProject.Tasks.Task9
{
    public static class EnumExtension
    {
        public static bool IsDeploymentSuccess(this DeploymentResult result)
        {
            if (result == DeploymentResult.Created || result == DeploymentResult.Warnings)
            {
                return true;
            }
            else
                return false;
        }
    }

    public enum DeploymentResult
    {
        Created,
        Failed,
        TimedOut,
        Warnings
    };
}