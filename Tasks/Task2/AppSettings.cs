﻿using Microsoft.Extensions.Configuration;

namespace TrainingProject.Tasks.Task2
{
    public class AppSettings
    {
        public static string GetMyKeyFromApplicationSettings(IConfiguration configuration)
        {
            return configuration["MyKey"];
        }
    }
}
