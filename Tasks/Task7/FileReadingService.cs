﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingProject.Tasks.Task7
{
    public class FileReadingService
    {
        public async Task<string> ProcessReadAsync()
        {
            string fileToRead = ".\\Resources\\Test document.txt";

            return await File.ReadAllTextAsync(fileToRead);
        }
    }
}