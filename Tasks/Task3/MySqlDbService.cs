﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TrainingProject.Models;

namespace TrainingProject.Tasks
{
    public class MySqlDbService
    {
        private ApplicationContext db;
        public MySqlDbService(ApplicationContext db)
        {
            this.db = db;
        }
        public List<Person> GetAllPersons()
        {
            return db.Persons.ToList();
        }

        public List<Person> GetPersonByName(string name)
        {
            var persons = db.Persons.Where(p => p.Name == name).ToList();
            return persons;
        }

        public void AddPerson(Person person)
        {
            db.Persons.Add(person);
            db.SaveChanges();
        }

        public static List<Person> GetAllPersonsSQL()
        {
            string queryString = "SELECT * FROM [dbo].[Persons]";
            string connectionString = "Server=DESKTOP-L49LKKC; Database=trainingdb; Trusted_Connection=True";

            List<Person> persons = new List<Person>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        persons.Add(new Person((int)reader["Id"], (string)reader["Name"], (string)reader["Phone"], (bool)reader["IsFired"]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return persons;
        }

        public static List<Person> GetPersonsByNameSQL(string name)
        {
            string storedProcedureName = "GetPersonByName";
            string connectionString = "Server=DESKTOP-L49LKKC; Database=trainingdb; Trusted_Connection=True";

            List<Person> persons = new List<Person>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(storedProcedureName, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@name", name));
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        persons.Add(new Person((int)reader["Id"], (string)reader["Name"], (string)reader["Phone"], (bool)reader["IsFired"]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return persons;
        }
    }
}