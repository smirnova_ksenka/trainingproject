﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingProject.Services
{
    public class Operation : IOperation
    {
        public Operation(IConfiguration configuration)
        {
            OperationTime = DateTime.Now;
            MyKey = configuration["MyKey"];
        }

        public DateTime OperationTime { get; }

        public string MyKey { get; }
    }
}
