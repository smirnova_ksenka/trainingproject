﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingProject.Services
{
    public interface IOperation
    {
        DateTime OperationTime { get; }
        string MyKey { get; }
    }
}
